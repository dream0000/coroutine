
# Coroutines basics

coroutine 是一种并发编程技术，多个 coroutine 可以并发执行。

每个 coroutine 是一个可以 suspend 的 computation instance。 

coroutine 挂起之后不会阻塞其所在的线程，该线程可以继续运行其他 coroutine或其他非 coroutine 代码。

coroutine 可以不与特定的线程绑定，可能在一个线程挂起，然后在另一个线程恢复。


## Your first coroutine
```Kotlin
fun main() = runBlocking { // this: CoroutineScope
    launch { // launch a new coroutine and continue
        delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
        println("World!") // print after delay
    }
    println("Hello") // main coroutine continues while a previous one is delayed
}
```
输出结果：
```
Hello
World!
```

- launch 是一个 coroutine builder， 可以启动一个新的 coroutine。
- delay 是一个suspend function，它可以 suspend 当前 coroutine 指定的时长。 suspend function 只能在 suspend function或coroutine builder block中调用。
- runBlocking 也是一个coroutine builder，它创建一个 CoroutineScope，并在该 CoroutineScope 中启动一个 coroutine。 runBlocking 会阻塞调用线程，直到 CoroutineScope 中启动的所有 coroutine 运行完。
- runBlocking 与 launch 的区别是： 
  - runBlocking 是一个普通函数，而 launch 是 CoroutineScope 的extension function，所以调用 launch 需要一个 CoroutineScope 对象。
  - runBlocking 会阻塞调用线程， 而 launch 不会。


### Structured concurrency
coroutine 遵循 structured concurrency 原则：所有 coroutine 都要在一个特定的 CoroutineScope 中启动， CoroutineScope 限定了由它启动的所有 coroutine 的生命周期。

在上面的例子中， runBlocking创建了一个 CoroutineScope， 只有等CoroutineScope中的所有 coroutine都完成之后才会退出。

在实际开发中，经常会创建大量coroutine， structured concurrency 确保所有 coroutine 不会丢失和泄露。只有 CoroutineScope 中的所有 coroutine 都完成了， 该CoroutineScope 才会完成。 代码中的所有错误都会被正确上报，不会丢失， 不管 coroutine 嵌套了多少层， 内层 coroutine 抛出的异常都会一层层向外上报。

## Extract function refactoring
如果把一段代码抽取成function， 并且这段代码中调用了suspend函数， 则这个函数会自动被定义成suspend函数， 因为suspend函数只允许在suspend函数函数中调用，不能在普通函数中调用。

```Kotlin
fun main() = runBlocking { // this: CoroutineScope
    launch { doWorld() }
    println("Hello")
}

// this is your first suspending function
suspend fun doWorld() {
    delay(1000L)
    println("World!")
}
```


## Scope builder
除了使用 coroutine builder提供的 CoroutineScope， 还可以使用 coroutineScope 函数创建自己的 CoroutineScope。 coroutineScope 是一个 suspend function， 它会挂起当前的 coroutine， 直到它创建的 CoroutineScope 启动的所有 coroutine 全部结束。

```Kotlin
fun main() = runBlocking {
    doWorld()
}

suspend fun doWorld() = coroutineScope {  // this: CoroutineScope
    launch {
        delay(1000L)
        println("World!")
    }
    println("Hello")
}
```

runBlocking 和 coroutineScope 都会等待代码块及其内部所有 child coroutine 完成后才会返回，两者的区别是:
- runBlocking 是普通函数，会阻塞当前线程
- coroutineScope 是suspend 函数，不会阻塞线程，所以可以在任何 suspend 函数中使用coroutineScope。


## Scope builder and concurrency
通过在 coroutineScope 创建的 CoroutineScope 中启动多个 coroutine， 可以实现多任务并发。

```kotlin
// Sequentially executes doWorld followed by "Done"
fun main() = runBlocking {
    doWorld()
    println("Done")
}

// Concurrently executes both sections
suspend fun doWorld() = coroutineScope { // this: CoroutineScope
    launch {
        delay(2000L)
        println("World 2")
    }
    launch {
        delay(1000L)
        println("World 1")
    }
    println("Hello")
}
```

输出结果：
```
Hello
World 1
World 2
Done
```


## An explicit job
launch 会返回一个Job对象， Job是 coroutine 的handle，利用Job对象可以显式的等待一个 coroutine 结束。
```kotlin
fun main() = runBlocking {
    val job = launch { // launch a new coroutine and keep a reference to its Job
        delay(1000L)
        println("World!")
    }
    println("Hello")
    job.join() // wait until child coroutine completes
    println("Done")     
}
```
输出结果:
```
Hello
World!
Done
```

## Coroutines ARE light-weight
thread是一种重量级并发工具， 创建两三千个thread可能就会导致进程OOM， 而 coroutine 很轻量，启动10万个也不会消耗太多资源。

```kotlin
import kotlinx.coroutines.*

//sampleStart
fun main() = runBlocking {
    repeat(100_000) { // launch a lot of coroutines
        launch {
            delay(5000L)
            print(".")
        }
    }
}
//sampleEnd
```

# Cancellation and timeouts

## Cancelling coroutine execution
通常，在关闭一个页面的时候，我们需要把该页面启动的一些后台 coroutine 取消。 launch 返回的 Job 对象是 coroutine 的handle， 调用 Job 对象的 cancel() 方法就可以cancel coroutine 。

```Kotlin
fun main() = runBlocking {
    val job = launch {
        repeat(1000) { i ->
            println("job: I'm sleeping $i ...")
            delay(500L)
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancel() // cancels the job
    job.join() // waits for job's completion 
    println("main: Now I can quit.")    
}
```
输出结果:
```
job: I'm sleeping 0 ...
job: I'm sleeping 1 ...
job: I'm sleeping 2 ...
main: I'm tired of waiting!
main: Now I can quit.
```

## Cancellation is cooperative
cancel coroutine 需要 coroutine 代码的配合。 如果 coroutine 代码不配合， cancel是不起作用的。 ```kotlinx.coroutines``` 库中的所有 suspend function 都是 cancellable， 它们会检查当前 coroutine 是否被cancel了，如果是就会throw CancellationException。

下面是一个不配合cancel的例子：
```kotlin
fun main() = runBlocking {
    val startTime = System.currentTimeMillis()
    val job = launch(Dispatchers.Default) {
        var nextPrintTime = startTime
        var i = 0
        while (i < 5) { // computation loop, just wastes CPU
            // print a message twice a second
            if (System.currentTimeMillis() >= nextPrintTime) {
                println("job: I'm sleeping ${i++} ...")
                nextPrintTime += 500L
            }
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion
    println("main: Now I can quit.")    
}
```
在这个例子中，即便已经cancel了job，但是该 coroutine 仍然会继续运行，直到5次循环结束。

## Making computation code cancellable
有两种方法让代码变成cancellable：
- 周期性的调用会检查cancellation的suspend function, 比如 ```yield()```就是一个比较合适的选择。
- 显式检查cancellation状态。

下面是第二种方法的一个例子：
```kotlin
import kotlinx.coroutines.*

fun main() = runBlocking {
    val startTime = System.currentTimeMillis()
    val job = launch(Dispatchers.Default) {
        var nextPrintTime = startTime
        var i = 0
        while (isActive) { // cancellable computation loop
            // print a message twice a second
            if (System.currentTimeMillis() >= nextPrintTime) {
                println("job: I'm sleeping ${i++} ...")
                nextPrintTime += 500L
            }
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion
    println("main: Now I can quit.")    
}
```
在这个例子里用 ```while (isActive)```替换了```while (i < 5)```。

## Closing resources with finally
cancellable suspend function抛出的CancellationException 可以按常规方式进行处理。 所以可以使用 ```try {...} finally {...}``` 表达式或 ```use``` 函数进行正常的finalization处理。

```kotlin
import kotlinx.coroutines.*

fun main() = runBlocking {
    val job = launch {
        try {
            repeat(1000) { i ->
                println("job: I'm sleeping $i ...")
                delay(500L)
            }
        } finally {
            println("job: I'm running finally")
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion
    println("main: Now I can quit.")    
}
```
输出结果:
```
job: I'm sleeping 0 ...
job: I'm sleeping 1 ...
job: I'm sleeping 2 ...
main: I'm tired of waiting!
job: I'm running finally
main: Now I can quit.
```

## Run non-cancellable block
在上面的例子中，如果coroutine被cancel，在finally块中再调用suspend函数就会直接抛出 CancellationException。
如果确实需要调用suspend函数， 需要把这些代码放在一个 ```withContext(NonCancellable) {...}```中。
就像下面的例子：
```kotlin
import kotlinx.coroutines.*

fun main() = runBlocking {
    val job = launch {
        try {
            repeat(1000) { i ->
                println("job: I'm sleeping $i ...")
                delay(500L)
            }
        } finally {
            withContext(NonCancellable) {
                println("job: I'm running finally")
                delay(1000L)
                println("job: And I've just delayed for 1 sec because I'm non-cancellable")
            }
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion
    println("main: Now I can quit.")    
}
```
## Timeout
有时候我们需要给coroutine中某段代码的执行设置超时时间，如果超时则自动取消coroutine，
这时候可以使用 ```withTimeout()``` 函数。

例如：
```kotlin
import kotlinx.coroutines.*

fun main() = runBlocking {
    withTimeout(1300L) {
        repeat(1000) { i ->
            println("I'm sleeping $i ...")
            delay(500L)
        }
    }
}
```
这段代码会输出：
```
I'm sleeping 0 ...
I'm sleeping 1 ...
I'm sleeping 2 ...
Exception in thread "main" kotlinx.coroutines.TimeoutCancellationException: Timed out waiting for 1300 ms
```

```withTimeout()``` 实际上是启动一个coroutine运行代码块中的代码，并设置一个超时定时器，
如果定时器超时了coroutine还没完成，则cancel该coroutine，并抛出异常。

```withTimeout()```会在超时的情况下抛出```TimeoutCancellationException```，```TimeoutCancellationException```是```CancellationException```的子类。 我们以前没有看到过在console中打印```CancellationException```的stack trace，这是因为在cancelled coroutine中
```CancellationException```被认为是coroutine 结束的一个正常原因。
这里之所以会打印exception stack，是因为直接在```runBlocking```中调用了```withTimeout```，而```runBlocking```把异常抛出了。
如果是在```launch```中调用，就不会打印exception stack。

```TimeoutCancellationException``` 只是一个exception，所以所有的资源会按照通常的方式被关闭。 可以把 ```withTimeout()``` 代码块包在一个 ```try {...} catch (e: TimeoutCancellationException) {...}```代码块中来做一些额外的处理。还可以使用```withTimeoutOrNull```代替```withTimeout()```，```withTimeoutOrNull```会在超时的情况下返回null而不是抛出异常。


## Asynchronous timeout and resources
```withTimeout```触发的timeout event与 timeout 代码块是异步执行的，
可能在代码块儿执行的任何时刻发生，甚至正好在timeout代码块返回前发生。
如果正好在timeout代码块返回前发生，则```withTimeout```就不会返回结果，而是抛出异常。
所以，如果在timeout代码块儿中open resource，然后在代码块儿外面close，就需要小心。

```kotlin
var acquired = 0

class Resource {
    init { acquired++ } // Acquire the resource
    fun close() { acquired-- } // Release the resource
}

fun main() {
    runBlocking {
        repeat(100_000) { // Launch 100K coroutines
            launch { 
                val resource = withTimeout(60) { // Timeout of 60 ms
                    delay(50) // Delay for 50 ms
                    Resource() // Acquire a resource and return it from withTimeout block     
                }
                resource.close() // Release the resource
            }
        }
    }
    // Outside of runBlocking all coroutines have completed
    println(acquired) // Print the number of resources still acquired
}
```
上面的代码并不能保证总是输出0，因为有大量coroutine要返回Resource()对象，
有些coroutine在返回Resource对象时可能已经超过60ms了，withTimeout就会抛出异常，
不会执行resource.close(). 为了解决这个问题，我们可以在代码块儿中保存resource的引用，而不是由```withTimeout```返回resource引用，并用```try {...} finally {...}``` close resource。 就像下面这样：

```kotlin
runBlocking {
    repeat(100_000) { // Launch 100K coroutines
        launch { 
            var resource: Resource? = null // Not acquired yet
            try {
                withTimeout(60) { // Timeout of 60 ms
                    delay(50) // Delay for 50 ms
                    resource = Resource() // Store a resource to the variable if acquired      
                }
                // We can do something else with the resource here
            } finally {  
                resource?.close() // Release the resource if it was acquired
            }
        }
    }
}
// Outside of runBlocking all coroutines have completed
println(acquired) // Print the number of resources still acquired
```

# 组合suspending functions
介绍了组合使用suspending functions的各种方法

## Sequential by default

```kotlin
suspend fun doSomethingUsefulOne(): Int {
    delay(1000L) // pretend we are doing something useful here
    return 13
}

suspend fun doSomethingUsefulTwo(): Int {
    delay(1000L) // pretend we are doing something useful here, too
    return 29
}
```

```kotlin
val time = measureTimeMillis {
    val one = doSomethingUsefulOne()
    val two = doSomethingUsefulTwo()
    println("The answer is ${one + two}")
}
println("Completed in $time ms")
```

```
The answer is 42
Completed in 2017 ms
```

## Concurrent using async
```kotlin
val time = measureTimeMillis {
    val one = async { doSomethingUsefulOne() }
    val two = async { doSomethingUsefulTwo() }
    println("The answer is ${one.await() + two.await()}")
}
println("Completed in $time ms")
```

## Lazily started async
```kotlin
val time = measureTimeMillis {
    val one = async(start = CoroutineStart.LAZY) { doSomethingUsefulOne() }
    val two = async(start = CoroutineStart.LAZY) { doSomethingUsefulTwo() }
    // some computation
    one.start() // start the first one
    two.start() // start the second one
    println("The answer is ${one.await() + two.await()}")
}
println("Completed in $time ms")
```

## Async-style functions
```kotlin
// The result type of somethingUsefulOneAsync is Deferred<Int>
@OptIn(DelicateCoroutinesApi::class)
fun somethingUsefulOneAsync() = GlobalScope.async {
    doSomethingUsefulOne()
}

// The result type of somethingUsefulTwoAsync is Deferred<Int>
@OptIn(DelicateCoroutinesApi::class)
fun somethingUsefulTwoAsync() = GlobalScope.async {
    doSomethingUsefulTwo()
}
```

```kotlin
// note that we don't have `runBlocking` to the right of `main` in this example
fun main() {
    val time = measureTimeMillis {
        // we can initiate async actions outside of a coroutine
        val one = somethingUsefulOneAsync()
        val two = somethingUsefulTwoAsync()
        // but waiting for a result must involve either suspending or blocking.
        // here we use `runBlocking { ... }` to block the main thread while waiting for the result
        runBlocking {
            println("The answer is ${one.await() + two.await()}")
        }
    }
    println("Completed in $time ms")
}
```

## Structured concurrency with async
利用```coroutineScope ```可以实现结构化并发，也就是在coroutine之间建立父子关系。
```kotlin
suspend fun concurrentSum(): Int = coroutineScope {
    val one = async { doSomethingUsefulOne() }
    val two = async { doSomethingUsefulTwo() }
    one.await() + two.await()
}
```

如果某个coroutine抛出异常，异常会沿着coroutine hierarchy向上传播，整个coroutine hierarchy都会被自动cancel。 例如：
```kotlin
import kotlinx.coroutines.*

fun main() = runBlocking<Unit> {
    try {
        failedConcurrentSum()
    } catch(e: ArithmeticException) {
        println("Computation failed with ArithmeticException")
    }
}

suspend fun failedConcurrentSum(): Int = coroutineScope {
    val one = async<Int> { 
        try {
            delay(Long.MAX_VALUE) // Emulates very long computation
            42
        } finally {
            println("First child was cancelled")
        }
    }
    val two = async<Int> { 
        println("Second child throws an exception")
        throw ArithmeticException()
    }
    one.await() + two.await()
}
```
输出结果：
```
Second child throws an exception
First child was cancelled
Computation failed with ArithmeticException
```