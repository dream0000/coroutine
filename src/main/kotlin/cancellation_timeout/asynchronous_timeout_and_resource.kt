package cancellation_timeout

import kotlinx.coroutines.*

var acquired = 0
var closed = 0

class Resource {
    init {
        acquired++
    } // Acquire the resource

    fun close() {
        closed++
    } // Release the resource
}

fun main() {
    var exceptionCount = 0
    runBlocking {
        repeat(100_000) { // Launch 100K coroutines
            launch {
                try {
                    val resource = withTimeout(60) { // Timeout of 60 ms
                        delay(50) // Delay for 50 ms
                        Resource() // Acquire a resource and return it from withTimeout block
                    }
                    resource.close() // Release the resource
                } catch (e: TimeoutCancellationException) {
                    exceptionCount++
                }
            }
        }
    }
    println("timeout exception count: $exceptionCount")
    // Outside of runBlocking all coroutines have completed
    println("acquired=$acquired, closed=$closed") // Print the number of resources still acquired
}