package coroutine_basics.light_weight.thread

import kotlin.concurrent.thread

//sampleStart
fun main() {
    repeat(100_000) { // launch a lot of coroutines
        println(it)
        thread {
            Thread.sleep(5000L)
            print(".")
        }
    }
}
//sampleEnd